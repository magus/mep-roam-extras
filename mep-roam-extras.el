;;; mep-roam-extras.el --- Some extra stuff for roam -*- lexical-binding: t -*-

;; Author: Magnus Therning
;; Maintainer: Magnus Therning
;; Version: 0
;; Package-Requires: ((org) (org-roam) (dash "2.19.1"))
;; Homepage: none
;; Keywords: org roam


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Some extra stuff for org-roam

;;; Code:

(require 'dash)
(require 'org)
(require 'org-roam)



;; functions to deal with TODO items in roam files
;; inspired by  https://d12frosted.io/posts/2021-01-16-task-management-with-roam-vol5.html

(defun mep-roam-extras--todo-p ()
  "Return non-nil if current buffer has any TODO entry.

TODO entries marked as done are ignored, meaning the this
function returns nil if current buffer contains only completed
tasks."
  (org-element-map
      (org-element-parse-buffer 'headline)
      'headline
    (lambda (h)
      (eq (org-element-property :todo-type h)
          'todo))
    nil 'first-match))

(defun mep-roam-extras--get-filetags ()
  (--> (org-roam-get-keyword "filetags")
       (or it "")
       (split-string it ":")
       (-filter (-compose #'not #'string-blank-p) it)))

(defun mep-roam-extras--add-filetag (tag)
  (let* ((new-tags (cons tag (mep-roam-extras--get-filetags)))
         (new-tags-str (--> new-tags
                            (combine-and-quote-strings it ":")
                            (format ":%s:" it))))
    (org-roam-set-keyword "filetags" new-tags-str)))

(defun mep-roam-extras--del-filetag (tag)
  (let* ((new-tags (seq-difference (mep-roam-extras--get-filetags) `(,tag)))
         (new-tags-str (--> new-tags
                            (combine-and-quote-strings it ":")
                            (format ":%s:" it))))
    (org-roam-set-keyword "filetags" new-tags-str)))

(defun mep-roam-extras--todo-files ()
  "Return a list of roam files containing todo tag."
  (org-roam-db-sync)
  (let ((todo-nodes (seq-filter (lambda (n)
                                  (seq-contains-p (org-roam-node-tags n) "todo"))
                                (org-roam-node-list))))
    (seq-uniq (seq-map #'org-roam-node-file todo-nodes))))

;;;###autoload
(defun mep-roam-extras-update-todo-tag ()
  "Update TODO tag in the current buffer."
  (when (and (not (active-minibuffer-window))
	     (org-roam-file-p))
    (org-with-point-at 1
      (let* ((tags (mep-roam-extras--get-filetags))
	     (is-todo (mep-roam-extras--todo-p)))
        (cond ((and is-todo (not (seq-contains-p tags "todo")))
	       (mep-roam-extras--add-filetag "todo"))
	      ((and (not is-todo) (seq-contains-p tags "todo"))
	       (mep-roam-extras--del-filetag "todo")))))))

;;;###autoload
(defun mep-roam-extras-update-todo-files (&rest _)
  "Update the value of `org-agenda-files'."
  (setq org-agenda-files (mep-roam-extras--todo-files)))

(provide 'mep-roam-extras)

;;; mep-roam-extras.el ends here
